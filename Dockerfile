FROM python:3.9-alpine as back
ENV CRYPTOGRAPHY_DONT_BUILD_RUST=1
ENV PYTHONUNBUFFERED 1
ENV PATH="/scripts:${PATH}"
ARG POETRY_VERSION=1.1.4

RUN pip install --upgrade pip

RUN apk add --update --no-cache \
        postgresql-client \
        jpeg-dev
RUN apk add --update --no-cache --virtual .tmp-build-deps \
        gcc \
        libressl-dev \
        libc-dev \
        linux-headers \
        postgresql-dev \
        musl-dev \
        zlib \
        zlib-dev \
        libffi-dev

RUN pip3 install --no-cache-dir poetry==${POETRY_VERSION}

RUN mkdir /app
COPY ./bundles /app/bundles
COPY ./backend /app/backend
COPY ./scripts /scripts
COPY ./pyproject.toml ./poetry.lock /app/

WORKDIR /app/backend

RUN poetry config virtualenvs.create false
RUN poetry install --no-dev

RUN apk del .tmp-build-deps

RUN chmod +x /scripts/*

RUN mkdir -p /vol/web/media
RUN mkdir -p /vol/web/static
RUN adduser -D user
RUN chown -R user:user /vol/
RUN chmod -R 755 /vol/web
USER user

CMD ["entrypoint.sh"]
