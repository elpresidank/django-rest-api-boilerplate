variable "prefix" {
  # prefix for all the aws resources
  default = "cd"
}

variable "project" {
  default = "codedank-app"
}

variable "contact" {
  default = "admin@codedank.com"
}

# vars for the RDS database
variable "db_username" {
  description = "Username for the RDS postgres instance"
}

variable "db_password" {
  description = "Password for the RDS postgres instance"
}

# name of key (ssh) for the bastion server
variable "bastion_key_name" {
  default = "codedank-app-bastion"
}


# ECR image uri for app
#   note: overwritten in gitlab ci for latest push
variable "ecr_image_app" {
  description = "ECR image for APP"
  # pull latest tag
  default = "832907639880.dkr.ecr.us-east-1.amazonaws.com/codedank-app:latest"
}

# ECR image uri for proxy
variable "ecr_image_proxy" {
  description = "ECR image for proxy"
  default     = "832907639880.dkr.ecr.us-east-1.amazonaws.com/codedank-proxy:latest"
}

variable "django_secret_key" {
  description = "Secret key for Django app"
  # no default, set in gitlab project variables
}

# zone name from route 53
variable "dns_zone_name" {
  description = "Domain name"
  default     = "codedank.com"
}

variable "subdomain" {
  description = "Subdomain per environment"
  type        = map(string)
  default = {
    production = "www"
    staging    = "staging"
    dev        = "dev"
  }
}


