data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name = "name"
    # get from EC2 AMIs with the instance arn
    # provide * to pull the most recent image
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }
  owners = ["amazon"]
}

# new iam role - assume role for bastion
#   *** NEEDED TO ASSIGN POLICIES TO EC2 INSTANCES
resource "aws_iam_role" "bastion" {
  name               = "${local.prefix}-bastion"
  assume_role_policy = file("./templates/bastion/instance-profile-policy.json")

  tags = local.common_tags
}

# attach policy to the bastion
#   - can attach whatever policies are needed
#   - only need to pull container images to ec2 from ecr
#     => can run django management commands
resource "aws_iam_role_policy_attachment" "bastion_attach_policy" {
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
  role       = aws_iam_role.bastion.name
}

# create instance profile
#   - contains the role to assign to bastion
resource "aws_iam_instance_profile" "bastion" {
  name = "${local.prefix}-bastion-instance-profile"
  role = aws_iam_role.bastion.name
}

# ec2 instance
resource "aws_instance" "bastion" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

  # run script to install dependencies on bastion server when created
  user_data = file("./templates/bastion/user-data.sh")

  # assign instance profile to server
  iam_instance_profile = aws_iam_instance_profile.bastion.name

  # bastion key pair name (ec2)
  key_name = var.bastion_key_name

  # subnet for the ec2 instance to launch into
  #   - only needed on a; not critical service, only admin purposes
  subnet_id = aws_subnet.public_a.id

  # add security group to server
  vpc_security_group_ids = [
    aws_security_group.bastion.id
  ]

  # set tags to instance
  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-bastion")
  )
}

# security group for the bastion server
#   => lock down scope of inbound/outbound traffic
resource "aws_security_group" "bastion" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"

  # assign security group to the vpc
  vpc_id = aws_vpc.main.id

  /* DEFINE GROUP RULES */
  # inbound access (SSH)
  ingress {
    # port range
    from_port = 22
    protocol  = "tcp"
    to_port   = 22
    # any ip address, could set to static ip (company building, recommended)
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound access to world 443-HTTPS 80-HTTP (package manager updates & ECR)
  egress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    protocol    = "tcp"
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  # outbound access (postgres)
  egress {
    from_port = 5432
    protocol  = "tcp"
    to_port   = 5432

    # specify private subnets running the db
    #   - ports only accessable through private subnet
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block
    ]
  }

  # add additional resources running within private subnet (specific ports)

  tags = local.common_tags
}
