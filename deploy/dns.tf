################
# Hosted Zones #
################

# retrieve zone for domain
data "aws_route53_zone" "zone" {
  name = "${var.dns_zone_name}."
}


#################
# API Subdomain #
#################

# record for load balancer
#     records added allow subdomains
resource "aws_route53_record" "app" {
  # hosted zone
  zone_id = data.aws_route53_zone.zone.zone_id
  # record name
  name = "${lookup(var.subdomain, terraform.workspace)}.${data.aws_route53_zone.zone.name}"

  # existing dns name, redirect
  type = "CNAME"

  # time to live
  ttl = "300"

  records = [aws_lb.app.dns_name]
}

# api certificate (ssl)
resource "aws_acm_certificate" "cert" {
  # fully qualified domain name
  domain_name = aws_route53_record.app.fqdn

  validation_method = "DNS"

  tags = local.common_tags

  # keep terraform running smooth when destroying
  lifecycle {
    # recommended to prevent errors
    create_before_destroy = true
  }
}

# cert validation record
resource "aws_route53_record" "cert_validation" {
  name    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_name
  type    = aws_acm_certificate.cert.domain_validation_options.0.resource_record_type
  zone_id = data.aws_route53_zone.zone.zone_id
  records = [
    aws_acm_certificate.cert.domain_validation_options.0.resource_record_value
  ]

  # shortest possible time
  ttl = "60"
}

# not a real resource
# triggers validation process in aws
resource "aws_acm_certificate_validation" "cert" {
  # cert to validate
  certificate_arn = aws_acm_certificate.cert.arn

  # validation record
  validation_record_fqdns = [aws_route53_record.cert_validation.fqdn]
}
