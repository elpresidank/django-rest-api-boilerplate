# allows us to add multiple subnets to the database
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# security group within network
#   => controls inbound (and outbound) access to db resource
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  # controls rules for inbound access
  ingress {
    from_port = 5432
    protocol  = "TCP"
    to_port   = 5432

    # limit access to resources with specific security group
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id,
    ]
  }

  tags = local.common_tags
}

# RDS database for codedank
# NOTE: ADJUST THE FIELDS FOR A PRODUCTION DB
resource "aws_db_instance" "main" {
  # db identifier (for console and aws)
  identifier = "${local.prefix}-db"
  # db name within postgres instance
  name = "codedank"
  # disk space for db (GB)
  allocated_storage = 20
  # ssd general purpose 2 (entry level, cheap)
  storage_type   = "gp2"
  engine         = "postgres"
  engine_version = "11.6"
  # type of db server
  instance_class = "db.t2.micro"
  # group for private subnets
  db_subnet_group_name = aws_db_subnet_group.main.name
  password             = var.db_password
  username             = var.db_username
  # number of days to maintain backups for, make at least 7
  backup_retention_period = 0
  # determines if db should run on multiple avail zones, make true for prod
  multi_az = false
  # snapshot of db when resource is deleted
  skip_final_snapshot = true
  # set permissions for accessing db through the network
  vpc_security_group_ids = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
