# create s3 bucket to store the media files
resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files"

  # access control list
  acl = "public-read"

  #   cors_rule {
  #     allowed_methods = ["GET"]
  #     allowed_origins = ["*"]
  #     max_age_seconds = 3000
  #     allowed_headers = ["Authorization"]
  #   }
  #   cors_rule {
  #     allowed_methods = ["POST"]
  #     allowed_origins = ["https://${aws_acm_certificate.studio_cert.domain_name}"]
  #     max_age_seconds = 3000
  #     allowed_headers = ["Authorization"]
  #   }

  # destroy bucket with tf, no input needed
  force_destroy = true
}
