# define Elastic Container Service resources

# create ECS cluster (1 per environment)
#    - will contain all tasks and services
resource "aws_ecs_cluster" "main" {
  name = "${local.prefix}-cluster"

  tags = local.common_tags
}

/* Assign permission to start tasks */

# create iam policy for task execution role
#   => will give ecs permissions to start new task
resource "aws_iam_policy" "task_execution_role_policy" {
  name = "${local.prefix}-task-exec-role-policy"
  # route path (organizes policies)
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  policy      = file("./templates/ecs/task-exec-role.json")
}

# create iam role
resource "aws_iam_role" "task_execution_role" {
  name = "${local.prefix}-task-exec-role"

  # pass in assume role policy
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}

# attach policy to the role
resource "aws_iam_role_policy_attachment" "task_execution_role" {
  # pass in the role
  role = aws_iam_role.task_execution_role.name

  # pass in the policy arn
  policy_arn = aws_iam_policy.task_execution_role_policy.arn
}

/* Giving permission to task at run time */
resource "aws_iam_role" "app_iam_role" {
  name = "${local.prefix}-app-task"

  # same assume role policy as task exec role
  assume_role_policy = file("./templates/ecs/assume-role-policy.json")

  tags = local.common_tags
}
/* --- */

# create log group for task logs (cloudwatch)
resource "aws_cloudwatch_log_group" "ecs_task_logs" {
  name = "${local.prefix}-app"

  tags = local.common_tags
}

# template file for container definitions
data "template_file" "app_container_definitions" {
  template = file("./templates/ecs/container-definitions.json.tpl")

  # variables to be passed in to container definition
  vars = {
    # image URIs
    app_image   = var.ecr_image_app
    proxy_image = var.ecr_image_proxy
    # set via environment variables
    django_secret_key = var.django_secret_key
    db_host           = aws_db_instance.main.address
    db_name           = aws_db_instance.main.name
    db_user           = aws_db_instance.main.username
    db_pass           = aws_db_instance.main.password
    log_group_name    = aws_cloudwatch_log_group.ecs_task_logs.name
    log_group_region  = data.aws_region.current.name
    # route 53 fqdn
    allowed_hosts = aws_route53_record.app.fqdn

    # dynamic urls
    app_url = aws_route53_record.app.fqdn

    # file storage env vars
    aws_storage_bucket_name = aws_s3_bucket.app_public_files.bucket
    aws_s3_region_name      = data.aws_region.current.name

    aws_user_name = "codedank-app-ci"
    tmp           = "tmp"
  }
}

# create task definition
resource "aws_ecs_task_definition" "app" {
  # name of the task definition
  family = "${local.prefix}-app"
  # get rendered container definition from template file
  container_definitions = data.template_file.app_container_definitions.rendered

  # make ECS task compatible with fargate
  #   => type of ECS hosting, severless deployment
  requires_compatibilities = ["FARGATE"]

  # use containers with the vpc
  network_mode = "awsvpc"

  # set cpu and memory allocation
  cpu    = 256
  memory = 512

  # role that gives permissions to execute new task
  execution_role_arn = aws_iam_role.task_execution_role.arn
  # role given to actual running task
  task_role_arn = aws_iam_role.app_iam_role.arn

  volume {
    name = "static"
  }

  tags = local.common_tags
}

# create security group to give access to service
resource "aws_security_group" "ecs_service" {
  description = "Access for the ECS service"
  name        = "${local.prefix}-ecs-service"
  vpc_id      = aws_vpc.main.id

  # allow outbound internet access from container on port 443 (HTTPS)
  egress {
    from_port   = 443
    protocol    = "tcp"
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # allow outbound access from service to database
  egress {
    from_port = 5432
    protocol  = "tcp"
    to_port   = 5432
    # private subnet with database
    cidr_blocks = [
      aws_subnet.private_a.cidr_block,
      aws_subnet.private_b.cidr_block,
    ]
  }

  # allow inbound access from internet on port 8000 (proxy running)
  #   - all connections must go via proxy to application (8000)
  ingress {
    from_port = 8000
    protocol  = "tcp"
    to_port   = 8000
    # only allow access in through the load balancer
    security_groups = [aws_security_group.lb.id]
  }

  tags = local.common_tags
}

# define ecs service for the app
resource "aws_ecs_service" "app" {
  name = "${local.prefix}-app"
  # assign ecs cluster and task definition
  cluster         = aws_ecs_cluster.main.name
  task_definition = aws_ecs_task_definition.app.family
  # number of tasks to run inside service
  #   * increase as traffic grows
  desired_count = 1
  # run tasks without managing servers
  launch_type = "FARGATE"

  # put in private subnet
  network_configuration {
    subnets = [
      aws_subnet.private_a.id,
      aws_subnet.private_b.id,
    ]
    security_groups = [aws_security_group.ecs_service.id]
  }

  # define lb
  #   - tells ecs service to register new tasks with target group
  load_balancer {
    target_group_arn = aws_lb_target_group.app.arn
    # forward to proxy
    container_name = "proxy"
    container_port = 8000
  }

  # wait for https listener
  depends_on = [aws_lb_listener.app_https]
}

# template file data resource for s3 bucket
data "template_file" "ecs_s3_write_policy" {
  template = file("./templates/ecs/s3-write-policy.json.tpl")

  vars = {
    bucket_arn = aws_s3_bucket.app_public_files.arn
  }
}

# policy with the template file for s3
resource "aws_iam_policy" "ecs_s3_access" {
  name        = "${local.prefix}-AppS3AccessPolicy"
  path        = "/"
  description = "Allow access to the codedank S3 bucket"

  # pass in policy from template file
  policy = data.template_file.ecs_s3_write_policy.rendered
}

# attach policy to the task role
resource "aws_iam_role_policy_attachment" "ecs_s3_access" {
  role       = aws_iam_role.app_iam_role.name
  policy_arn = aws_iam_policy.ecs_s3_access.arn
}
