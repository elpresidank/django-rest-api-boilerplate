# * TF: Outputs of a certain variable or attribute on a resource

# set output of db address
#   => see in pipeline console, used for configuring and debugging server
#      can also be used to connect in through the bastion server
output "db_host" {
  # internal network address of the db
  value = aws_db_instance.main.address
}

# output bastion hostname
output "bastion_host" {
  # public dns name for the bastion server, needed for admin purposes
  value = aws_instance.bastion.public_dns
}

# output app endpoint
# **************
output "app_endpoint" {
  value = aws_route53_record.app.fqdn
}
