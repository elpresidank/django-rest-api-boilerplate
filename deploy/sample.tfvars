# template of variables for local development
#   => used with terraform.tfvars

db_username = "codedank_app"

# local dev, change before you run
db_password = "changeme"

django_secret_key = "changeme"
