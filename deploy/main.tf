terraform {
  # declare the backend for the terraform, bucket for TF state and DB for tf lock
  backend "s3" {
    bucket         = "codedank-app-tfstate"
    key            = "codedank-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "codedank-app-tfstate-lock"
  }
}

# provider for terraform
provider "aws" {
  region  = "us-east-1"
  version = "~> 2.54.0"
}


locals {
  # dynamic variable prefix for aws resources
  prefix = "${var.prefix}-${terraform.workspace}"

  # tags to apply to all resources where possible
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

# get current region from aws
data "aws_region" "current" {}
