from django.test import TestCase
from django.contrib.auth import get_user_model

User = get_user_model()


class UsersManagersTests(TestCase):

    def test_create_user(self):
        """Test creating a new users with an email is successful"""
        user = User.objects.create_user(email='normal@users.com',
                                        username='TestUser', password='foo')
        self.assertEqual(user.email, 'normal@users.com')
        self.assertEqual(user.username, 'TestUser')
        self.assertTrue(user.is_active)
        self.assertFalse(user.is_staff)
        self.assertFalse(user.is_superuser)
        try:
            """ Username does not exist for the AbstractBaseUser option"""
        except AttributeError:
            pass
        with self.assertRaises(TypeError):
            User.objects.create_user()
        with self.assertRaises(TypeError):
            User.objects.create_user(email='')
        with self.assertRaises(ValueError):
            User.objects.create_user(email='', username='', password='foo')

    def test_new_user_email_normalized(self):
        """Test the email for a new users is normalized"""
        email = 'test@CODEDANK.COM'
        user = get_user_model().objects.create_user(
            email=email, username='TestUser1', password='test123')

        self.assertEqual(user.email, email.lower())

    def test_create_superuser(self):
        """Test creating a new superuser"""
        admin_user = User.objects.create_superuser(
            email='super@users.com',
            username='TestUser2',
            password='foo'
        )
        self.assertEqual(admin_user.email, 'super@users.com')
        self.assertEqual(admin_user.username, 'TestUser2')
        self.assertTrue(admin_user.is_active)
        self.assertTrue(admin_user.is_staff)
        self.assertTrue(admin_user.is_superuser)
        try:
            """ Username does not exist for the AbstractBaseUser option"""
        except AttributeError:
            pass
        with self.assertRaises(ValueError):
            User.objects.create_superuser(email='super@users.com',
                                          username='TestUser3',
                                          password='foo',
                                          is_superuser=False)
