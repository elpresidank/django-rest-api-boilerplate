import uuid
from django.db import models


class BaseModel(models.Model):
    """Model to be inherited by all models except User"""
    uuid = models.UUIDField(
        default=uuid.uuid4,
        editable=False,
        unique=True,
    )
    timestamp = models.DateTimeField(auto_now_add=True, blank=True,
                                     null=True)
    updated = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True
