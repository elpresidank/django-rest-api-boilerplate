import uuid
import os
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.utils.translation import ugettext_lazy as _
from django.db import models


def avatar_file_path(instance, filename):
    """Generate file path for new avatar"""
    ext = filename.split('.')[-1]
    filename = f'{instance.uuid}.{ext}'

    return os.path.join(f'users/{instance.username}/avatars/', filename)


def cover_file_path(instance, filename):
    """Generate file path for new cover"""
    ext = filename.split('.')[-1]
    filename = f'{instance.uuid}.{ext}'

    return os.path.join(f'users/{instance.username}/covers/', filename)


class UserManager(BaseUserManager):
    """Custom users model manager where email is the unique
     identifiers for authentication instead of usernames."""

    def create_user(self, email, username, password=None, **extra_fields):
        """Creates and saves a new users with the given email and password."""
        if not email:
            raise ValueError(_('Users must have an email address'))
        if not username:
            raise ValueError(_('Users must have a username'))
        user = self.model(email=self.normalize_email(email), **extra_fields)
        user.username = username
        user.set_password(password)
        user.save(using=self._db)

        return user

    def create_superuser(self, email, username, password, **extra_fields):
        """Creates and saves a new super users with the given email and
        password"""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_active', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError(_('Superuser must have is_staff=True.'))
        if extra_fields.get('is_superuser') is not True:
            raise ValueError(_('Superuser must have is_superuser=True.'))
        return self.create_user(email=email, username=username,
                                password=password, **extra_fields)


class User(AbstractUser):
    """Custom User model that supports using email instead of username"""
    email = models.EmailField(_('email address'), unique=True)
    uuid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __str__(self):
        return self.email
