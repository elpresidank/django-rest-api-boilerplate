from django.contrib.auth import forms as auth_forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError

User = get_user_model()


class CustomPasswordChangeFrom(auth_forms.SetPasswordForm):
    """Custom SetPassword Form"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['new_password1'].widget.attrs.update(
            {'placeholder': _('New Password')})
        self.fields['new_password1'].help_text = (
            'Your password can’t be too similar to your '
            'other personal information.',
            'Your password must contain at least 8 characters.',
            'Your password can’t be a commonly used password.',
            'Your password can’t be entirely numeric.',
        )
        self.fields['new_password2'].widget.attrs.update(
            {'placeholder': _('Confirm New Password')})
        self.fields['new_password2'].help_text = (
            'Enter the same password as before, for verification.',
        )


class CustomPasswordResetForm(auth_forms.PasswordResetForm):
    """Custom Password Reset Form"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'placeholder': _('Email')})

    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email__iexact=email,
                                   is_active=True).exists():
            raise ValidationError(
                "There is no user registered with the "
                "specified email address!")

        return email


class CustomUserLoginForm(auth_forms.AuthenticationForm):
    """Custom User Authentication form"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'placeholder': _('Email')})
        self.fields['password'].widget.attrs.update(
            {'placeholder': _('Password')})


class CustomUserCreationForm(auth_forms.UserCreationForm):
    """Custom User creation form for our custom User model"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'placeholder': _('Email')})
        self.fields['email'].help_text = ('Please enter a valid email.',)
        self.fields['username'].widget.attrs.update(
            {'placeholder': _('Username')})
        self.fields['username'].help_text = (
            'Required. 150 characters or fewer. Letters, digits and'
            ' @/./+/-/_ only.',)
        self.fields['password1'].widget.attrs.update(
            {'placeholder': _('Password')})
        self.fields['password1'].help_text = (
            'Your password can’t be too similar to your other'
            ' personal information.',
            'Your password must contain at least 8 characters.',
            'Your password can’t be a commonly used password.',
            'Your password can’t be entirely numeric.',
        )
        self.fields['password2'].widget.attrs.update(
            {'placeholder': _('Confirm Password')})
        self.fields['password2'].help_text = (
            'Enter the same password as before, for verification.',
        )

    class Meta(auth_forms.UserCreationForm):
        model = User
        fields = ('email', 'username')


class CustomUserChangeForm(auth_forms.UserChangeForm):
    """User change form for our custom User model"""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['username'].widget.attrs.update(
            {'placeholder': _('Username')})
        self.fields['email'].widget.attrs.update({'placeholder': _('Email')})

    class Meta:
        model = User
        fields = ('email', 'username')
