# accounts/views.py
from django.urls import reverse_lazy
from django.views import generic
from django.contrib.auth import views as auth_views
from django.contrib.auth import login, authenticate
from .forms import CustomUserCreationForm, CustomUserLoginForm, \
    CustomPasswordResetForm, CustomPasswordChangeFrom, CustomUserChangeForm


class PasswordResetConfirmView(auth_views.PasswordResetConfirmView):
    """Custom Password reset confirm view for our CustomPasswordChangeForm"""
    form_class = CustomPasswordChangeFrom


class PasswordResetView(auth_views.PasswordResetView):
    """Custom Password reset view for our CustomPasswordResetForm"""
    form_class = CustomPasswordResetForm


class LoginView(auth_views.LoginView):
    """Custom Login view for our CustomLoginForm"""
    form_class = CustomUserLoginForm
    template_name = 'registration/login.html'


class SignUpView(generic.CreateView):
    """Custom SignUp View for our custom user model"""
    form_class = CustomUserCreationForm
    success_url = reverse_lazy('home')
    template_name = 'registration/signup.html'

    def form_valid(self, form):
        valid = super(SignUpView, self).form_valid(form)
        email, password = form.cleaned_data.get(
            'username'), form.cleaned_data.get('password1')
        new_user = authenticate(username=email, password=password)
        login(self.request, new_user)
        return valid


class UserSettingsView(generic.FormView):
    form_class = CustomUserChangeForm
    success_url = reverse_lazy('settings')
    template_name = 'users/user_settings.html'
