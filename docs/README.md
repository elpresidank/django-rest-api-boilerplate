Dank App Boilerplate
====================
The goal of this application is to integrate several cutting edge web 
development frameworks for easy development, staging and production deployments.

> Structure options and naming conventions for the Dank App Boilerplate

### Top-level directory layout
    .
    ├── backend                   # Directory where all Django code resides.
    ├── bundles                   # Compiled output of static assets from frontend (Javascript and CSS).
    ├── frontend                  # Frontend directory where all SCSS and TypeScript for the project resides.
    ├── scripts                   # Devops shell scripts
    ├── .dockerignore
    ├── .gitignore
    ├── docker-compose.yml
    ├── Dockerfile
    ├── Dockerfile.dev
    ├── poetry.lock
    ├── pyproject.toml
    └── README.md

> Use short lowercase names at least for the top-level files and folders except
> `README.md`, `Dockerfile` and `Dockerfile.dev`

### Backend layout
    .
    ├── apps                          # Directory where all Django apps reside.
    │    ├── core                     # The core django app is to be used for abstract models, utility functions and non object related media files
    │    │      ├── management        # All custom django commands will be stored in this folders sub dir called "commands"
    │    │      └── static            # All non model related media files will be stored in this directory (.png, .jpg, .jpeg, .svg, .mp4, .webm, etc...)       
    │    ├── users                    # The users django app is to be used for all user related models, views and urls   
    │    └── __init__.py
    ├── conf                          # Dango configurations directory will hold settings files and base urls.py file
    │    ├── settings
    │    │    ├── __init__.py         # File that specifies which settings module to use based on environment
    │    │    ├── local.py            # Django development settings module
    │    │    └── prod.py             # Django production ready settings module
    │    ├── __init__.py
    │    ├── asgi.py               
    │    ├── urls.py                  # Django base urls.py file use this to include all new apps to the urlpatterns of the project
    │    └── wsgi.py
    ├── templates                     # Directory where all project templates reside (HTML). Each Django app will have a corresponding sub directory when applicable.
    ├── tests                         # Directory where all Django related tests will be kept each app will have a corresponding sub directory for relevant app related tests
    ├── __init__.py
    └── manage.py
> Use short lowercase names for the backend-level files and folders

### Frontend layout
    .
    ├── node_modules                   # Library root
    ├── src                            # Frontend Source files
    │    ├── components                # All TypeScript Components (Generally React Components)
    │    ├── pages                     # All Page related TypeScript entry points
    │    ├── scss                      # Project Styles (All .scss files)
    │    ├── utilities                 # All TypeScript Utility functions
    │    └── index.ts                 # Base TypeScript entry point
    ├── .babelrc                       # Babel configuration
    ├── .gitignore                  
    ├── .prettierrc                    # Prettier configurations
    ├── package.json                   # Frontend project configurations and dependencies
    ├── tsconfig.json                  # TypeScript configurations
    ├── webpack.dev.ts                 # Development webpack configurations
    ├── webpack.prod.ts                # Production webpack configuration
    └── yarn.lock                      # Dependency lock file
> Use short lowercase names at least for the frontend-level files and folders except for React Components i.e (`App.tsc`)


### How to Run Project
1. In Project root run `cd frontend`
2. In frontend run `yarn`
3. In frontend run `cd ..`
4. create a .env file in the project root with the following contents
   ```dotenv
    DJANGO_SECRET_KEY=djangosecretkey
    DB_HOST=db
    DB_NAME=backend
    DB_USER=postgres
    DB_PASS=supersecretpassword
    ENV=dev
    DEBUG=1
    ```
5. In project root run `docker-compose build`
6. After build run `docker-compose up`
7. If project doesn't run wait 5 seconds and re-run `docker-compose up`
8. To create super user run `docker-compose run --rm app sh -c "python manage.py createsuperuser"`
9. To Test and lint project run `docker-compsoe run --rm app sh -c "python manage.py test && flake8"`

Reference SASS.md and BEM.md for SCSS guidelines

Reference STACK.md for project stack specifications
