Dank App Boilerplate Stack
==========================
## Code (JavaScript)
- **Framework**: [React](https://reactjs.org/)
- **Superset**: [Typescript](https://www.typescriptlang.org/)
- **Bundler**: [Webpack](https://webpack.js.org/)
- **Testing**: [Jest](https://jestjs.io/)
- **Linting**: [Eslint](https://eslint.org/)
- **Code Formatter**: [Prettier](https://prettier.io/)
- **Package Manager**: [Yarn](https://classic.yarnpkg.com/en/)

## Styles (CSS)
- **Preprocessor**: [SASS](https://sass-lang.com/install)
- **Syntax**: [SCSS](https://sass-lang.com/install)

## Code (Python)
- **Framework**: [django](https://www.djangoproject.com/)
- **Testing**: [pytest](https://docs.pytest.org/en/stable/)
- **Linting**: [flake8](https://flake8.pycqa.org/en/latest/)
- **Code Formatter**: [black](https://black.readthedocs.io/en/stable/)
- **Code Coverage**: [coverage](https://coverage.readthedocs.io/en/coverage-5.3.1/)
- **Database**: [postgresql](https://www.postgresql.org/)
- **Caching**: [redis](https://redis.io/)
- **Reverse Proxy**: [nginx](https://www.nginx.com/)
- **HTTP2/Websocket Protocol Server**: [daphne](https://pypi.org/project/daphne/0.8.1/)
- **HTTP Protocol Server**: [gunicorn](https://gunicorn.org/)  
- **Websocket Support**: [django channels](https://channels.readthedocs.io/en/stable/)
- **Task Queuing**: [celery](https://docs.celeryproject.org/en/stable/)
- **Dependency Management**: [Poetry](https://python-poetry.org/)

## DevOps
- **Infrastructure as Code**: [terraform](https://www.terraform.io/)
- **CI/CD**: [gitlab](https://about.gitlab.com/)
- **Cloud Platform**: [amazon web services](https://aws.amazon.com/)
- **Containerization**: [docker](https://www.docker.com/)

## Email
- **Amazon SES**: [aws simple email service](https://aws.amazon.com/ses/)