## BEM Cheatsheet

### BLOCK

Block encapsulates a standalone entity that is meaningful on its own.

While blocks can be nested and interact with each other, semantically they remain equal; there is no precedence or hierarchy.

Holistic entities without DOM representation (such as controllers or models) can be blocks as well. 

#### Naming
Block name may consist of Latin letters, digits, and dashes.

To form a CSS class, add short prefix for namespacing:
**.b-block**

We typically use `b-` as a prefix, `b` stands for `'Brandwatch'` or `'block'`, depending on what you like better.

#### Note on naming prefixes

We use **b-** as a class name prefix; you can choose your own, or go without any prefixes at all.

#### HTML
Any DOM node can be a block if it accepts a class name.

```html
<div class="b-block">...</div>
```

#### CSS for blocks
  * Use class name selector only
  * No tag names or id's
  * No dependency on other blocks/elements on a page

```css
.b-block {
    color: #042;
}
```

### ELEMENT

Elements are parts of a block and have no standalone meaning.
Any element is semantically tied to its block.

#### HTML
Any DOM node within a block can be an element.
Within a given block, all elements are semantically equal.

#### Naming
Element name may consist of Latin letters, digits, and dashes.

CSS class is formed as block name + two undercores + element name:
**.b-block__elem**

```html
<div class="b-block">
  ...
  <span class="b-block__elem"></span>
</div>
```

#### CSS for elements
  * Use class name selector only
  * No tag name or id's
  * No dependency on other blocks/elements on a page

```css
  /* BAD */ .b-block .b-block__elem { border: solid 1px #000 }
  /* BAD */        div.b-block__elem { border: solid 1px #000 }
  
  /* GOOD  */         .b-block__elem { border: solid 1px #000 }
```

### MODIFIER

Modifiers are flags on blocks or elements.
Use them to change appearance or behavior.

#### HTML
Modifier is an extra class name which you add to a block/element DOM node.

#### Naming
Modifiers (both keys and values) may consist of Latin letters, digits, and dashes.

Modifier can be a boolean flag or a key/value pair.
Naming conventions:

  * Boolean modifiers:<br>
    Original block/element name + double dash + mod name<br>
    **.b-block--mod** or **.b-block__elem--mod**
  * Key/value modifiers:<br>
    Original block/element name + double dash + mod key name + single underscore + mod value<br>
    **.b-block--key_value** or **.b-block__elem--key_value**

Add modifier classes only to blocks/elements they modify, and keep the original class:

```html
  GOOD <div class="b-block b-block--mod">...</div>
  GOOD <div class="b-block b-block--size_xl b-block--shadow_yes">...</div>
  
  BAD  <div class="b-block--mod">...</div>
```

#### CSS for modifiers
Use modifier class name as selector:

```css
.b-block--hidden {
    display: none;
}
```

To alter elements based on a block-level modifier:

```css
.b-block--mod .b-block__elem {
    display: none;\
}
```

Element modifier:

```css
.b-block__elem--mod {
    display: none;
}
```