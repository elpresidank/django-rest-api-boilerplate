Dank App Boilerplate React and TypeScript Design Patterns
=========================================================

### Basic function components
 When using function components without any props you don’t have the need to
 use extra types. Everything can be inferred. In old-style functions as well as
 arrow functions
 
```
const Title = () => {
  return <h1>Welcome to this application</h1>;
}
```

### Props
 When using props we name props usually according to the component we’re
 writing, with a Props-suffix. No need to use FC component wrappers or
 anything similar.
```
type GreetingProps = {
  name: string;
};

const Greeting = (props: GreetingProps) => {
  return <p>Hi {props.name} 👋</p>
}
```
Destructuring makes it even more readable
```
const Greeting = ({ name }: GreetingProps) => {
  return <p>Hi {name} 👋</p>;
}
```

### Default props
Instead of setting default props, like in class-based React, it’s easier
to set default values to props. We mark props with a default value optional
(see the question-mark operator). The default value makes sure that **name** is
never undefined.
```
type LoginMsgProps = {
  name?: string;
};

const LoginMsg = ({ name = "Guest" }: LoginMsgProps) => {
  return <p>Logged in as {name}</p>;
}
```

### Children
Instead of using `FC` or `FunctionComponent` helpers we prefer to set 
`children` explicitly, so it follows the same pattern as other components. We
set `children` to type `React.ReactNode` as it accepts most (JSX elements,
strings, etc.)
```
type CardProps = {
  title: string;
  children: React.ReactNode;
};

export const Card = ({ title, children }: CardProps) => {
  return (
    <section className="cards">
      <h2>{title}</h2>
      {children}
    </section>
  );
}
```
When we set `children` explicitly, we can also make sure that we never pass
any children.
```
// This throws errors when we pass children
type SaveButtonProps = {
  //... whatever
  children: never
}
```

### WithChildren helper type
A custom helper type helps us to set `children` easier.
```
type WithChildren<T = {}> = 
  T & { children?: React.ReactNode };

type CardProps = WithChildren<{
  title: string;
}>;
```
This is very similar to `FC`, but with the default generic parameter to `{}`, it
can be much more flexible:
```
// works as well
type CardProps = { title: string } & WithChildren;
```

### Spread attributes to HTML elements
Spreading attributes to HTML elements is a nice feature where you can make sure
that you are able to set all the HTML properties that an element has without
knowing upfront which you want to set. You pass them along. Here’s a button
wrapping component where we spread attributes. To get the right attributes, we
access a `button's` props through `JSX.IntrinsicElements`. This includes `children`,
we spread them along.
```
type ButtonProps = JSX.IntrinsicElements["button"];

function Button({ ...allProps }: ButtonProps) {
  return <button {...allProps} />;
}
```

### Preset attributes
Let’s say we want to preset `type` to `button` as the default behavior `submit` tries
to send a form, and we just want to have things clickable. We can get type safety
by omitting `type` from the set of button props.
```
type ButtonProps =
  Omit<JSX.IntrinsicElements["button"], "type">;

const Button = ({ ...allProps }: ButtonProps) => {
  return <button type="button" {...allProps} />;
}

// 💥 This breaks, as we omitted type
const z = <Button type="button">Hi</Button>; 
```

### Required properties
We dropped some props from the type definition and preset them to sensible defaults.
Now we want to make sure our users don’t forget to set some props. Like the alt
attribute of an image or the `src` attribute.

For that, we create a `MakeRequired` helper type that removes the optional flag.

```
type MakeRequired<T, K extends keyof T> = Omit<T, K> &
  Required<{ [P in K]: T[P] }>;
```
And build our props with that:

```
type ImgProps 
  = MakeRequired<
    JSX.IntrinsicElements["img"], 
    "alt" | "src"
  >;

export const Img = ({ alt, ...allProps }: ImgProps) => {
  return <img alt={alt} {...allProps} />;
}

const zz = <Img alt="..." src="..." />;
```