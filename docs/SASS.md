Dank App Boilerplate SCSS Architecture, and Best Practice Reference
===================================================================

## File Structure
    . 
    ├── abstracts                    # Abstract related directory (variables, functions, mixins)
    │    ├── __abstracts-dir.scss    # Import all abstract partials in the abstracts directory into this file
    │    ├── _functions.scss         # Include all scss functions in this partial
    │    ├── _mixins.scss            # Include all scss mixins in this partial
    │    └── _variables.scss         # Include all scss variables in this partial
    ├── base                         # Base related directory (animations, typography, resets, utilities)
    │    ├── __base-dir.scss         # Import all base partials in the base directory into this file
    │    ├── _animations.scss        # Include all animation rules in this partial
    │    ├── _base.scss              # Include all resets and base rules in this partial
    │    ├── _typography.scss        # Include all typography related rules in this partial
    │    └── _utilities.scss         # Include all utility css classes in this partial
    ├── components                   # Components related directory
    │    ├── __components-dir.scss   # Import all components partials in the components directory into this file
    │    └── ...                     # Etc.
    ├── layouts                      # Layouts related directory (grid, footer, header, etc...)
    │    ├── __layouts-dir.scss      # Import all layouts partials in the layouts directory into this file
    │    └── ...                     # Etc.
    ├── pages                        # Pages related directory (landing-page, home-page, etc...)
    │    ├── __pages-dir.scss        # Import all pages partials in the pages directory into this file
    │    └── ...                     # Etc.
    └── main.scss                    # HomePage Scss File

* Categorized styles; making them easier to maintain 
*  5-1 Pattern architecture; for clearer organization (based on the 7-1 Pattern)
*  Clear delineation between grouped and associated rules 
*  Encourages common style, components and variable (inline with Atomic Design)
*  Modular style management that facilitates Styleguide Driven Development 
*  Reusable approach

## main.scss
* Charset should be included
* Only directory files `-dir.scss` files should be imported
* Changes should not be made to this file
* Filename is always ‘styles’ as it is a compiled file containing multiple stylesheets
* All files except /styles.scss require an underscore at the beginning of each filename

## Abstracts
* Use the `__abstracts-dir.scss`
* Helper functions and non–output snippets only (font-face imports, reusable mixins, global variables, colors, etc.)
* One `@import` per line
* The following files are imported as standard ready for customization:
    * `_functions.scss`
    * `_mixins.scss`
    * `_variables.scss`
* Abstracts should be grouped with an inline comment title
* One abstract per line
* C-style comments can be used to add context
* Files with no data should not be removed

## Base
* Use the `__base-dir.scss`
* Global styles are imported here
* A custom reset file can be added to override normalize
* Typography (`h1-6`, `p`, `a`, `blockquote`, etc.) should be included
* Globally defined tags (`body`, `main`, `article`, `div`, `etc.`) should also be included
* One `@import` per line
* The following files are imported as standard ready for customization:
    * `_animations.scss`
    * `_base.scss`
    * `_reset.scss`
    * `_typography.scss`
    * `_utilities.scss`
* Additional base stylesheets can be imported

## Components
* Use the `__components-dir.scss`
* Micro level reusable components are imported here
* Each component does one thing and one thing only
* Components are re-usable across the project
* Components are independent
* One `@import` per line
* Each stylesheet contains all component styles, variations and states

## Layouts
* Use the `__layouts-dir.scss`
* Unique combinations of components and base styles are imported here
* One `@import` per line
* Each stylesheet contains all component style overrides, and specific layout only attributes

## Pages
* Use the `__pages-dir.scss`
* Unique combinations of components and base styles are imported here
* One `@import` per line
* Each stylesheet contains all component style overrides, and specific page only attributes

## Additional Stylesheets
* Ensure additional stylesheets are added in the appropriate folder and imported correctly in the directories -dir.scss file.

Example of `__components-dir.scss`

```scss
/*
This file is used to contain all component imports.
Files inside this folder should contain all styles relating to a reusable component.
*/

//Import Component files
@import "button";
@import "input";
@import "modal";
@import "accordion";
@import "dropdown";
@import "carousel";
@import "form";
@import "icon";
@import "label";
```

## Writing Rules
* Two (2) spaces indents, no tabs
* Single line whitespace between rules
* No more than 2 levels of nesting
* Reference BEM.md for further reference