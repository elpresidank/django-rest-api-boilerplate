import path = require("path");
import webpack = require("webpack");
import { CleanWebpackPlugin } from "clean-webpack-plugin";
import IgnoreEmitPlugin from "ignore-emit-webpack-plugin";
import MiniCssExtractPlugin = require("mini-css-extract-plugin");
import CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
import { WebpackManifestPlugin } from "webpack-manifest-plugin";
import ForkTsCheckerWebpackPlugin = require("fork-ts-checker-webpack-plugin");

const config: webpack.Configuration = {
  mode: "production",
  devtool: "source-map",
  entry: {
    app: "./src/index.ts",
    styles: "./src/scss/main.scss",
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        include: path.resolve(__dirname, "src/scss"),
        use: [MiniCssExtractPlugin.loader, "css-loader", "sass-loader"],
      },
      {
        test: /\.(ts|js)x?$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader",
          options: {
            presets: [
              "@babel/preset-env",
              "@babel/preset-typescript",
            ],
          },
        },
      },
      {
        test: /\.(png|jpe?g|gif)$/i,
        loader: "file-loader",
        options: {
          name: "assets/img/[name].[ext]",
        },
      },
      {
        test: /\.(svg)$/,
        loader: "file-loader",
        options: {
          name: "assets/img/icons/[name].svg",
        },
      },
    ],
  },
  resolve: {
    extensions: [".tsx", ".ts", ".js", ".jsx"],
  },
  output: {
    publicPath: "",
    filename: "[name].[contenthash].js",
    path: path.resolve(__dirname, "../bundles"),
  },
  plugins: [
    new CleanWebpackPlugin(),
    new ForkTsCheckerWebpackPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].[contenthash].css",
      chunkFilename: "[id].[contenthash].css",
      ignoreOrder: true,
    }),
    new IgnoreEmitPlugin(/styles.*.js$/),
    // @ts-ignore
    new WebpackManifestPlugin(),
  ],
  optimization: {
    moduleIds: "deterministic", // Use to keep vendor hash consistent between builds
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
        styles: {
          name: "styles",
          test: /\.css$/,
          chunks: "all",
          enforce: true,
        },
      },
    },
    minimizer: ["...", new CssMinimizerPlugin()],
  },
};

export default config;
